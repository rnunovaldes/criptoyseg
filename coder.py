import re
import math
import unicodedata
from abc import ABC, abstractmethod
from ansii import Printer, Color

def factorization(num,max_factor=-1):
    """
    Encuentra los factores de un numero

    Args:
      num: Un natural a encontrar sus factores -> int
      max_factor: indica el factor maximo que debe de listarse -> int

    Return:
      Una lista con los factores del numero
    """
    factors = []
    natural = abs(num)
    s = math.floor(math.sqrt(natural))
    if max_factor != -1:
        if s > max_factor: s = max_factor
    for i in range(2,s+1):
        if (natural % i) == 0:
            if i not in factors: factors.append(i)
            result = int(natural/i)
            if (max_factor != -1) and (result <= max_factor):
                if result not in factors: factors.append(result)
    return sorted(factors)

def mcd(a,b):
    """
    Encuentra el minimo comun divisor para dos numeros

    Args:
      a: Un numero -> int
      b: Otro numero -> int

    Return:
      El minimo comun multiplo como entero -> int
    """
    while b != 0:
        temp = b
        b = a % b
        a = temp
    return int(a)

def det(matrix):
    """
    Encuentra el determinante de una matriz (2x2 de momento)

    Args:
      matrix: Una matriz cuadrada -> list

    Return:
      El determinante de una matriz -> int
    """
    return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0]

def multiply(matrix_a,matrix_b,module=-1):
    """
    Multiplica dos matrices

    Args:
      matrix_a: Una matriz cuadrada -> list
      matrix_b: Una matriz (lista) de nx1 (len list = n)
      module: Un entero que se usara para aplicar a los
              resultados % el numero -> int

    Return:
      La matriz (lista) resultante -> list
    """
    matrix_c = []
    for i in range(len(matrix_a)):
        product = 0
        for j in range(len(matrix_b)):
            product += matrix_a[i][j] * matrix_b[j]
        matrix_c.append(product)
    if module != -1:
        matrix_c = [(item % module) for item in matrix_c]
    return matrix_c

def inverse(matrix,module=-1):
    """
    Calcula la matriz invesra de una matriz

    Args:
      matrix: Una matriz cuadrada -> list
      module: Un entero que se usara para aplicar a los
        resultados % el numero -> int

    Return:
      La matriz inversa resultante -> list?
    """
    d = det(matrix)
    if d == 0:
        return None
    d = 1/d
    inverse = [[int( d * matrix[1][1]),int(-d * matrix[0][1])],
               [int(-d * matrix[1][0]),int( d * matrix[0][0])]]
    if module != -1:
        for i in range(len(inverse)):
            inverse[i] = [(item % module) for item in inverse[i]]
    return inverse

def d2l(frecs):
    """
    Convierte un diccionaro a una lista para poder ordenarla de
    mayor a menor valor

    Args:
      frecs: Un diccionario con valores y llaves -> dict

    Return:
      Una lista de tuplas ordenada de acuerdo los valores -> list
    """
    L = []
    for k in list(frecs.keys()):
        L.append((k,frecs[k]))
    L.sort(key=lambda x: x[1], reverse=True)
    return L

def adjust_length(text, n=16, fill_character='0'):
    """
    Genera una cadena que será de tamaño congruente módulo n donde sus
    primeros caracteres serán los mismo que en el texto dado

    Args:
      text: El texto del que se tomarán los primeros caracteres -> str
      n: El número con el que se comparará la longitud del texto -> int
         Por defecto es 16
      fill_character: El caracter con el que se rellenará de
                      ser necesario -> str
    """
    adjust_text = text
    adjust_leng = len(text) % n
    if adjust_leng != 0:
        adjust_text = text.ljust(len(text) + (n - adjust_leng), fill_character)
    return adjust_text

def message_format_hexadecimal(text,is_hex=True):
    """
    Convierte una cadena de texto plano a una cadena de hexadecimales,
    [!agrega CRLF en su final si no lo tiene] y rellena con ceros en caso
    de no llegar a un múltiplo de 8 bytes (16 dígitos hexadecimales o
    64 bits)

    Args:
      text: El texto a convertir -> str
      is_hex: Indica si el texto ya está en hexadecimal y solo cambia
              los caracteres por mayúsculas de ser necesario.
              Si se indica que está en hexadecimal y no lo está, lo
              procesa como texto plano. Por defecto True -> bool

    Return:
      Una cadena de números hexadecimales -> str
    """
    text_hex = ''
    if is_hex:
        try:
            int(text, 16)
            text_hex = text.upper()
        except ValueError: text_hex = text.encode('utf-8').hex().upper()
    else: text_hex = text.encode('utf-8').hex().upper()
    #eol = '0D0A' # CRLF \r\n
    #if not text_hex.endswith(eol):
    #    text_hex += eol
    return adjust_length(text_hex)

def string_to_substrings(text, length=16):
    """
    Genera una lista de subcadenas dada una cadena donde todos sus
    elementos son de un tamaño dado

    Args:
      text: El texto a separar en subcadenas -> str
      length: El tamaño de los elementos en la lista
              Por defecto 16 -> int

    Return:
      Una lista de cadenas -> list
    """
    return [text[i:i+length] for i in range(0, len(text), length)]

class Coder(ABC):
    """
    Clase que tiene  varias funciones utiles para cifrar
    y decifrar textos
    """
    punctuation = '''¡!¿?！？{[()]}-.,;:'" \n\t'''

    _PC_1 = [57,49,41,33,25,17, 9,
              1,58,50,42,34,26,18,
             10, 2,59,51,43,35,27,
             19,11, 3,60,52,44,36,
             63,55,47,39,31,23,15,
              7,62,54,46,38,30,22,
             14, 6,61,53,45,37,29,
             21,13, 5,28,20,12, 4]

    _PC_2 = [14,17,11,24, 1, 5,
              3,28,15, 6,21,10,
             23,19,12, 4,26, 8,
             16, 7,27,20,13, 2,
             41,52,31,37,47,55,
             30,40,51,45,33,48,
             44,49,39,56,34,53,
             46,42,50,36,29,32]

    _one_leftshift = (1,2,9,16)

    _IP = [58,50,42,34,26,18,10, 2,
           60,52,44,36,28,20,12, 4,
           62,54,46,38,30,22,14, 6,
           64,56,48,40,32,24,16, 8,
           57,49,41,33,25,17, 9, 1,
           59,51,43,35,27,19,11, 3,
           61,53,45,37,29,21,13, 5,
           63,55,47,39,31,23,15, 7]

    _E_BIT_SELECTION = [32, 1, 2, 3, 4, 5,
                         4, 5, 6, 7, 8, 9,
                         8, 9,10,11,12,13,
                        12,13,14,15,16,17,
                        16,17,18,19,20,21,
                        20,21,22,23,24,25,
                        24,25,26,27,28,29,
                        28,29,30,31,32, 1]

    _SBOXES = ([14, 4,13, 1, 2,15,11, 8, 3,10, 6,12, 5, 9, 0, 7,
                 0,15, 7, 4,14, 2,13, 1,10, 6,12,11, 9, 5, 3, 8,
                 4, 1,14, 8,13, 6, 2,11,15,12, 9, 7, 3,10, 5, 0,
                15,12, 8, 2, 4, 9, 1, 7, 5,11, 3,14,10, 0, 6,13],
               [15, 1, 8,14, 6,11, 3, 4, 9, 7, 2,13,12, 0, 5,10,
                 3,13, 4, 7,15, 2, 8,14,12, 0, 1,10, 6, 9,11, 5,
                 0,14, 7,11,10, 4,13, 1, 5, 8,12, 6, 9, 3, 2,15,
                13, 8,10, 1, 3,15, 4, 2,11, 6, 7,12, 0, 5,14, 9],
               [10, 0, 9,14, 6, 3,15, 5, 1,13,12, 7,11, 4, 2, 8,
                13, 7, 0, 9, 3, 4, 6,10, 2, 8, 5,14,12,11,15, 1,
                13, 6, 4, 9, 8,15, 3, 0,11, 1, 2,12, 5,10,14, 7,
                 1,10,13, 0, 6, 9, 8, 7, 4,15,14, 3,11, 5, 2,12],
               [ 7,13,14, 3, 0, 6, 9,10, 1, 2, 8, 5,11,12, 4,15,
                13, 8,11, 5, 6,15, 0, 3, 4, 7, 2,12, 1,10,14, 9,
                10, 6, 9, 0,12,11, 7,13,15, 1, 3,14, 5, 2, 8, 4,
                 3,15, 0, 6,10, 1,13, 8, 9, 4, 5,11,12, 7, 2,14],
               [ 2,12, 4, 1, 7,10,11, 6, 8, 5, 3,15,13, 0,14, 9,
                14,11, 2,12, 4, 7,13, 1, 5, 0,15,10, 3, 9, 8, 6,
                 4, 2, 1,11,10,13, 7, 8,15, 9,12, 5, 6, 3, 0,14,
                11, 8,12, 7, 1,14, 2,13, 6,15, 0, 9,10, 4, 5, 3],
               [12, 1,10,15, 9, 2, 6, 8, 0,13, 3, 4,14, 7, 5,11,
                10,15, 4, 2, 7,12, 9, 5, 6, 1,13,14, 0,11, 3, 8,
                 9,14,15, 5, 2, 8,12, 3, 7, 0, 4,10, 1,13,11, 6,
                 4, 3, 2,12, 9, 5,15,10,11,14, 1, 7, 6, 0, 8,13],
               [ 4,11, 2,14,15, 0, 8,13, 3,12, 9, 7, 5,10, 6, 1,
                13, 0,11, 7, 4, 9, 1,10,14, 3, 5,12, 2,15, 8, 6,
                 1, 4,11,13,12, 3, 7,14,10,15, 6, 8, 0, 5, 9, 2,
                 6,11,13, 8, 1, 4,10, 7, 9, 5, 0,15,14, 2, 3,12],
               [13, 2, 8, 4, 6,15,11, 1,10, 9, 3,14, 5, 0,12, 7,
                 1,15,13, 8,10, 3, 7, 4,12, 5, 6,11, 0,14, 9, 2,
                 7,11, 4, 1, 9,12,14, 2, 0, 6,10,13,15, 3, 5, 8,
                 2, 1,14, 7, 4,10, 8,13,15,12, 9, 0, 3, 5, 6,11])

    _P = [16, 7,20,21,
          29,12,28,17,
           1,15,23,26,
           5,18,31,10,
           2, 8,24,14,
          32,27, 3, 9,
          19,13,30, 6,
          22,11, 4,25]

    _IP_INV = [40, 8,48,16,56,24,64,32,
               39, 7,47,15,55,23,63,31,
               38, 6,46,14,54,22,62,30,
               37, 5,45,13,53,21,61,29,
               36, 4,44,12,52,20,60,28,
               35, 3,43,11,51,19,59,27,
               34, 2,42,10,50,18,58,26,
               33, 1,41, 9,49,17,57,25]

    def __init__(self,cipher,key,alphabet=None):
        """
        Constructor de la clase

        Args:
          cipher: El nombre del cifrado -> str
          alphabet: Todas las letras que usa el cifrado. Si el cifrado no usa un
                    alfabeto fijo, por defecto es None -> str
          key: Algo que representa la llave que se usa para el cifrado
              -> int|(int,int)|str|(str,int)|(str,str)|(int,int,int,int)

        Returns:
          Un objeto que instancia a Cipher
        """
        self._cipher = cipher.lower()
        self._alphabet = self._preprocess(alphabet) if alphabet != None else None
        self._key = key
        self._size = 0
        self._gkey = None
        self._text = ''
        self._ctext = ''

    def _preprocess(self,text):
        """
        Sirve para preprocesar una cadena de texto, eliminar algunos
        caracteres especiales como acentos y volverla en mayusculas

        Args:
          text: El terxto a procesar -> str

        Returns:
          Una cadena con el texto ya procesado -> str
        """
        if self._cipher == 'des': return text.strip()
        mayus = text.upper()
        for x in self.punctuation:
            mayus = mayus.replace(x,'')
        no_accents = ''
        for c in mayus:
            if c == 'Ñ': no_accents += c
            else: no_accents += unicodedata.normalize('NFKD', c)\
                                .encode('ascii', 'ignore').decode('ascii')
        return no_accents

    def count_letters(self,ciph=False,order=False,as_percents=False):
        """
        Cuenta la cantidad de veces que aparece una letra en un texto
        de los asignados como texto sin cifrar o texto cifrado

        Args:
          ciph: Si el texto para contar sus letras es el texto
                cifrado o no -> bool
          order: Si se debe ordenar el conteo de letras o no -> bool
          as_percents: Si en lugar del conteo de letras, se prefieren
                       porcentajes -> bool

        Returns:
          El conteo de las letras -> dict
        """
        ref = self._ctext if ciph else self._text
        if ref == '': return None
        frecs = {}
        for letter in ref:
            frecs[letter] = frecs.get(letter, 0) + 1
        if as_percents:
            n = sum(frecs.values())
            for k in list(frecs.keys()):
                frecs[k] = round(frecs[k] / n,3)
        if order:
            return d2l(frecs)
        return frecs

    def index_of_coincidence(self,ciph=True):
        """
        Encuentra el indice de coincidencias en un texto. Una medida
        para saber que tan probable es que al escoger una pareja de
        letras al azar del texto, estas sean la misma

        Args:
          ciph: Si el texto al cual se le encontrara el indice de
                coincidencias es el texto cifrado o no -> bool

        Returns:
          El indice de coincidencia del texto -> double
        """
        letter_count = self.count_letters(ciph=ciph)
        N = sum(letter_count.values())
        ic = 0
        total = N * (N - 1)
        for letter in self._alphabet:
            n = letter_count.get(letter,0)
            ic += round((n * (n-1)) / total,4)
        return round(ic,4)

    def find_friedmann(self,ciph=True,calculate=False,roughness=0.0744):
        """
        Usando un nivel de aspereza (espanyol por defecto), estima el
        numero de alfabetos usados en un criptograma poliafabetico

        Args:
          ciph: Si el texto al cual se le encontraran el numero de
                alfabetos es el cifrado o no -> bool
          calculate: Si se tiene el texto original y se desea usar el
                     nivel de aspereza especifico de ese texto.
                     Toma preferencia a roughness -> bool
          roughness: El nivel de aspereza del idioma del cual, se
                     supone, viene el texto -> double
        """
        ref = self._ctext if ciph else self._text
        if ref == '': return None
        N = len(ref)
        unif_roughness = round(1 / len(self._alphabet),3)
        ic = self.index_of_coincidence(ciph)
        ciph_roughness = self.index_of_coincidence(False) if calculate\
                         else round(roughness,4)
        result = ((round(ciph_roughness - unif_roughness,3) * N)
                  / ((ic * (N - 1)) - (unif_roughness * N) + ciph_roughness))
        return round(result,4)

    def find_kasiski(self,size,ciph=True):
        """
        Encuentra subcadenas en un texto de una longitud dada y genera
        tres diccionarios que tienen los indices donde aparecen las
        subcadenas iguales, las distancias entre cada dos subcadenas
        iguales consecutivas y los factores en comun de las distancias

        Args:
          chiph: Si el texto a encontrar sus subcadenas es el texto
                 cifrado o no -> bool
          size: Entero que reresenta la longitud de las
                subcadenas a encontrar -> int

        Return:
          Tres diccionarios con las subcadenas encontradas y sus
          indices, distancias y factores de las distancias si la
          subcadena tiene mas de una ocurrencia -> (dict,dict,dict)
        """
        ref = self._ctext if ciph else self._text
        if ref == '': return None
        substrs = {}
        # Encuentra subcadenas
        for i in range(len(ref) - size + 1):
            substr = ref[i:i+size]
            if substr in substrs: substrs[substr].append(i)
            else: substrs[substr] = [i]
        # Elimina las de menos de una ocurrencia
        for key in list(substrs.keys()):
            if len(substrs.get(key)) == 1: del substrs[key]
        keys = list(substrs.keys())
        # Encuentra las distancias entre subcadenas iguales
        dsubstrs = substrs.copy()
        fsubstrs = substrs.copy()
        for key in keys:
            distance = []
            indexes = substrs.get(key)
            for i in range(len(indexes)-1):
                distance.append(indexes[i+1] - indexes[i])
            dsubstrs[key] = distance
        # Los factores de las distancias
            factors = []
            for d in distance:
                dfactors = factorization(d,len(self._alphabet))
                if not factors: factors = dfactors
                else: factors = list(set(factors) & set(dfactors))
            fsubstrs[key] = sorted(factors)
        return (substrs,dsubstrs,fsubstrs)

    @property
    def text(self):
        """
        Un getter para el texto sin cifrar

        Returns:
          Una cadena del texto sin cifrar -> str
        """
        return self._text

    @property
    def ctext(self):
        """
        Un getter para el texto cifrado

        Returns:
          Una cadena del texto cifrado -> str
        """
        return self._ctext

    @text.setter
    def text(self,value):
        """
        Un setter para el texto sin cifrar

        Args:
          value: Una cadena de texto para usar como texto sin
                 cifrar -> str
        """
        if isinstance(value,str):
            self._text = self._preprocess(value).strip()

    @ctext.setter
    def ctext(self,value):
        """
        Un setter para el texto cifrado

        Args:
          value: Una cadena de texto para usar como texto cifrado -> str
        """
        if isinstance(value,str):
            self._ctext = self._preprocess(value).strip()

    def read_from_file(self,path_file,is_cipher=False,in_input=True):
        """
        Un setter para el texto sin cifrar o cifrado que toma el
        contenido de un archivo

        Args:
          path: La ruta del archivo con el contenido -> str
          is_cipher: Si se le asignara el contenido del archivo al texto
                     cifrado -> bool
          in_input: Si el archivo a leer esta en la carpeta input, asi
                    que se usara 'input/path_file' -> bool
        """
        ref = 'input/'+path_file if in_input else path_file
        text_file = open(ref, 'r')
        read = self._preprocess(''.join(text_file.readlines()).strip())
        if is_cipher:
            self._ctext = read
        else:
            self._text = read
        text_file.close()

    def write_text_in_file(self,path_file='',is_cipher=True,in_output=True):
        """
        Guarda el texto del cifrado o sin cifrar en un archivo de texto

        Args:
          path_file: La ruta del archivo donde se guardara el texto. Si
                     no se asigna valor alguno, le dara el nombre
                     'nombre_del_cifrado-llave' y lo guardara en
                     output -> str
          is_cipher: Si se escribira el texto cifrado en el
                     archivo -> bool
          in_output: Si el archivo a leer esta en la carpeta output, asi
                     que se usara 'output/path_file' -> bool
        """
        ref = ''
        if path_file == '':
            ref = 'output/' + self._cipher + '-' +str(self._key) + '.txt'
        elif in_output: ref = 'output/'+path_file
        else: ref = path_file
        text_file = open(ref, 'w+')
        if is_cipher:
            text_file.write(self._format_text(True))
        else:
            text_file.write(self._format_text(False))
        text_file.close()

    def genkey(self,size=0):
        """
        Genera una llave para el cifrado si el atributo key ha
        sido definido

        Args:
          size: El tamanyo de la llave, si es requerido -> int
        """
        self._size = size
        if self._cipher == 'cesar':
            self._genkey_cesar()
        elif self._cipher == 'afin':
            self._genkey_afin()
        elif self._cipher == 'vigenere':
            self._genkey_vigenere()
        elif self._cipher == 'playfair':
            self._size = 5
            self._genkey_playfair()
        elif self._cipher == 'hill':
            self._size = 2
            self._genkey_hill()
        elif self._cipher == 'des':
            self._genkey_des()

    def _genkey_cesar(self):
        """
        Genera una llave para el cifrado de cesar. Esta llave es la
        cadena del alfabeto desplazada una cierta cantidad

        El atributo key es int
        """
        if not isinstance(self._key,int):
            raise TypeError(f'{self._key} is not int')
        key = self._key % len(self._alphabet)
        self._gkey = self._alphabet[key:]+self._alphabet[:key]

    def _genkey_afin(self):
        """
        Genera una llave para el cifrado afin

        El atributo key es tuple(int,int)
        """
        if not isinstance(self._key,tuple):
            raise TypeError(f'{self._key} is not tuple')
        alphlen = len(self._alphabet)
        if mcd(self._key[0],alphlen) == 1:
            self._gkey = (self._key[0]%alphlen,self._key[1]%alphlen,alphlen)
        else:
            raise ArithmeticError(f'({self._key[0]},{alphlen})!=1')

    def _genkey_vigenere(self):
        """
        Genera una llave para el cifrado de vigenere

        Si el atributo key es str, cifra con un el alfabeto que comienza
        con cada letra de la cadena; si es tuple, se espera que la
        segunda parte de esta tenga un numero que indique hasta cuantas
        letras de la cadena se ciclara
        """
        key = ''
        cicle = 0
        if isinstance(self._key,str):
            key = self._preprocess(self._key)
            cicle = len(self._key)
        elif isinstance(self._key,tuple):
            key = self._preprocess(self._key[0])
            cicle = self._key[1]
        else: raise TypeError(f'{self._key} is not str or tuple')
        vigenere = []
        for i in range(cicle):
            initial_letter = key[i % len(key)]
            index_letter = self._alphabet.index(initial_letter)
            vigenere.append(self._alphabet[index_letter:]
                            + self._alphabet[:index_letter])
        self._gkey = vigenere

    def _genkey_playfair(self):
        """
        Genera una llave para el cifrado de playfair. Esta llave es una
        matriz cuadrada

        Si el atributo key es str, se crea una matriz de izquierda a
        derecha y arriba hacia abajo con el orden de en que aparecen las
        letras y las ultimas 2 letras usan el mismo espacio;
        si es tuple, se espera que la segunda parte de esta tenga a las
        2 letras en particular que se combinan en el mismo espacio
        """
        key_list = []
        if isinstance(self._key,str):
            key_list = list(self._preprocess(self._key))
        elif isinstance(self._key,tuple):
            key_list = list(self._preprocess(self._key[0]))
        else: raise TypeError(f'{self._key} is not int or tuple')
        key=[]
        for i in range(len(key_list)):
            kpoped = key_list.pop(0)
            if kpoped not in key: key.append(kpoped)
        no_duplicates = [l for l in list(self._alphabet) if l not in key]
        order = []
        for i in range(len(self._alphabet)):
            try: order.append(key.pop(0))
            except IndexError: order.append(no_duplicates.pop(0))
        doble = ''
        if isinstance(self._key,tuple):
            doble = self._key[1][1].upper()
            order.remove(doble)
        else: doble = order.pop()
        playfair = []
        for i in range(self._size):
            row = []
            for j in range(self._size):
                row.append((order[(i*self._size)+j],None))
                if (isinstance(self._key,tuple)
                        and order[(i*self._size)+j] == self._key[1][0].upper()):
                    row[j] = (order[(i*self._size)+j],doble)
                elif (isinstance(self._key,str)) and ((i == self._size-1)
                        and (j == self._size-1)):
                    row[j] = (order[(i*self._size)+j],doble)
            playfair.append(row)
        self._gkey = playfair

    def _genkey_hill(self):
        """
        Genera una llave para el cifrado de hill. Esta llave es una
        matriz cuadrada

        Si el atributo key es str, acomoda los indices de las letras en
        la cadena para formar la matriz de derecha hacia abajo (rellena
        con la ultima letra del alfabeto, elimina sobrantes); si es
        tuple de int, los acomoda de derecha hacia abajo (rellena con 0,
        elimina sobrantes)
        """
        key = []
        alphlen = len(self._alphabet)
        if isinstance(self._key,str):
            for i in range(self._size**2):
                try:
                    letter = self._preprocess(self._key[i])
                    key.append(self._alphabet.index(letter))
                except IndexError: key.append(alphlen-1)
        elif isinstance(self._key,tuple):
            for i in range(self._size**2):
                try:
                    if not isinstance(self._key[i],int):
                        raise TypeError(f'{self._key} is not int or tuple')
                    key.append(self._key[i] % alphlen)
                except IndexError: key.append(0)
        else: raise TypeError(f'{self._key} is not str or tuple')
        hill = []
        for i in range(self._size):
            row = []
            for j in range(self._size):
                row.append(key[i * self._size + j])
            hill.append(row)
        determinant = det(hill)
        if mcd(determinant,alphlen) != 1:
            raise ValueError(f'({determinant},{alphlen})!=1')
        self._gkey = hill

    def _genkey_des(self):
        """
        Genera una llave para el cifrado DES. Esta llave es una cadena
        formada por números hexadecimales

        El atributo key es str; checa que tenga 16 o 64 caracteres y
        trata de convertirlo a int base 16 en el primer caso o convertirlo
        a int base 2 en el otro; si no puede no genera la llave.
        """
        key = []
        if isinstance(self._key,str):
            plain_key = self._key.replace(" ", "")
            if len(plain_key) == 16:
                try:
                    int(plain_key, 16)
                    key.append(plain_key.upper())
                except ValueError: raise ValueError(f'{self._key} is not hex')
            elif len(plain_key) == 64:
                try:
                    key.append(hex(int(plain_key, 2))[2:].zfill(16))
                except ValueError: raise ValueError(f'{self._key} is not bin')
            else: raise ValueError(f'{self._key} is not hex or bin valid size')
        else: raise TypeError(f'{self._key} is not str')
        key.append(bin(int(key[0], 16))[2:].zfill(len(key[0])*4))
        permuted_key = ''
        for b in self._PC_1:
            permuted_key += key[1][b-1]
        key.append(permuted_key)
        Cn = [key[2][:28]]
        Dn = [key[2][28:]]
        for i in range(1,17):
            n = 1 if i in self._one_leftshift else 2
            Cn.append(Cn[-1][n:]+Cn[-1][:n])
            Dn.append(Dn[-1][n:]+Dn[-1][:n])
        key.append((Cn,Dn))
        Kn = []
        for i in range(1,17):
            permuted_key = ''
            concat = key[3][0][i] + key[3][1][i]
            for b in self._PC_2:
                permuted_key += concat[b-1]
            Kn.append(permuted_key)
        key.append(Kn)
        self._gkey = key

    def print_key(self):
        """
        Imprime una llave generada para algun cifrado
        """
        Printer.printInfo('Llave:')
        if self._gkey == None: print('No genkey')
        if (self._cipher == 'cesar'):
            print(self._gkey,end='')
        elif self._cipher == 'afin':
            print(f'({self._gkey[0]}x+{self._gkey[1]})%{self._gkey[2]}',end='')
        elif self._cipher == 'vigenere':
            print('\n'.join(
                       [Color.colorize(Color.AZUL_NEGRITAS_BRILLANTE,b[0])
                       + b[1:] for b in self._gkey]))
        elif (self._cipher == 'playfair') or (self._cipher == 'hill'):
            for i in range(self._size):
                for j in range(self._size):
                    print(self._gkey[i][j], end='\t')
                print()
        elif self._cipher == 'des':
            print(Color.colorize(Color.BLANCO_NEGRITAS,'16HEX:'),self._gkey[0])
            print(Color.colorize(Color.BLANCO_NEGRITAS,'64BIT:'),
                  ' '.join([b[:-1]
                      + Color.colorize(Color.AMARILLO_BRILLANTE,b[-1])
                      for b in string_to_substrings(self._gkey[1],8)]))
            print(Color.colorize(Color.BLANCO_NEGRITAS,'56BIT:'),
                  Color.colorize(Color.AZUL_BRILLANTE,
                        ' '.join(string_to_substrings(self._gkey[2][:28],7))),
                  Color.colorize(Color.VERDE_BRILLANTE,
                        ' '.join(string_to_substrings(self._gkey[2][28:],7))))
            for i in range(1,17):
                print(Color.colorize(Color.BLANCO_NEGRITAS,
                      'C{:02d}:'.format(i)),
                      Color.colorize(Color.AZUL_BRILLANTE,
                        ' '.join(string_to_substrings(self._gkey[3][0][i],7))),
                      Color.colorize(Color.BLANCO_NEGRITAS,
                      'D{:02d}:'.format(i)),
                      Color.colorize(Color.VERDE_BRILLANTE,
                        ' '.join(string_to_substrings(self._gkey[3][1][i],7))))
            for i in range(16):
                print(Color.colorize(Color.BLANCO_NEGRITAS,
                      'K{:02d}:'.format(i+1)),
                      ' '.join(string_to_substrings(self._gkey[4][i],6)))
                '''Color.colorize(Color.AZUL_BRILLANTE,
                  ' '.join(string_to_substrings(self._gkey[4][i][:24],6))),
                Color.colorize(Color.VERDE_BRILLANTE,
                  ' '.join(string_to_substrings(self._gkey[4][i][24:],6))))'''
        print()

    def _format_text(self,cipher=False):
        text = self._ctext if cipher else self._text
        result = ''
        if (self._cipher == 'cesar') or (self._cipher == 'afin'):
            result = text
        if self._cipher == 'vigenere':
            for i in range(0,len(text),len(self._gkey)):
                result += text[i:i+len(self._gkey)] + ' '
        if self._cipher == 'playfair':
            for i in range(0,len(text),2):
                result += text[i:i+2] + ' '
        if self._cipher == 'hill':
            for i in range(0,len(text),self._size):
                result += text[i:i+self._size] + ' '
        if self._cipher == 'des':
            result = ' '.join([text[i:i+16] for i in range(0,len(text),16)])
        return result.strip()

    def print_text(self,cipher=False):
        """
        Imprime el texto cifrado o descifrado

        Args:
          cipher: Si se imprime el texto cifrado -> bool
        """
        text = ''
        if cipher:
            print('Texto cifrado:')
        else:
            print('Texto descifrado:')
        print(self._format_text(cipher=cipher),end='\n\n')

class Cipher(Coder):
    """
    Clase que puede cifrar textos
    """

    def cipher(self):
        """
        Sirve para cifrar el texto de acuerdo al cifrado especificado
        """
        if self._gkey == None: return
        if self._cipher == 'cesar':
            self.__cipher_cesar()
        elif self._cipher == 'afin':
            self.__cipher_afin()
        elif self._cipher == 'vigenere':
            self.__cipher_vigenere()
        elif self._cipher == 'playfair':
            self.__cipher_playfair()
        elif self._cipher == 'hill':
            self.__cipher_hill()
        elif self._cipher == 'des':
            self.__cipher_des()

    def __cipher_cesar(self):
        """
        Cifra el texto con el cifrado de cesar
        """
        self._ctext = ''
        for letter in self._text:
            self._ctext += self._gkey[self._alphabet.index(letter)]

    def __cipher_afin(self):
        """
        Cifra el texto con el cifrado de afin
        """
        self._ctext = ''
        func = lambda x: (self._gkey[0] * x + self._gkey[1]) % self._gkey[2]
        for letter in self._text:
            self._ctext += self._alphabet[func(self._alphabet.index(letter))]

    def __cipher_vigenere(self):
        """
        Cifra el texto con el cifrado de vigenere
        """
        self._ctext = ''
        i = 0
        for letter in self._text:
            self._ctext += self._gkey[i][self._alphabet.index(letter)]
            i = (i+1)%len(self._gkey)

    def __cipher_playfair(self):
        """
        Cifra el texto con el cifrado de playfair
        """
        self._ctext = ''
        tuples = []
        for i in range(0,len(self._text),2):
            try:
                #if self._text[i] == self._text[i+1]:
                #  tuples.append((self._text[i],'X'))
                #  tuples.append((self._text[i+1],'X'))
                #else:
                tuples.append((self._text[i],self._text[i+1]))
            except IndexError: tuples.append((self._text[i],'X'))
        for t in tuples:
            t1 = (0,0,False)
            t2 = (0,0,False)
            for i in range(self._size):
                for j in range(self._size):
                    if (self._gkey[i][j][0] == t[0]
                            or self._gkey[i][j][1] == t[0]):
                        t1 = (j,i,True)
                    if (self._gkey[i][j][0] == t[1]
                            or self._gkey[i][j][1] == t[1]):
                        t2 = (j,i,True)
                    if(t1[2] and t2[2]) == True: break
            # Misma letra en la pareja
            if (t1[0] == t2[0]) and (t1[1] == t2[1]):
                self._ctext += self._gkey[t1[1]][(t1[0]+1)%self._size][0] \
                               + self._gkey[t2[1]][(t2[0]+1)%self._size][0]
            # Misma fila
            elif t1[0] == t2[0]:
                self._ctext += self._gkey[(t1[1]+1)%self._size][t1[0]][0] \
                               + self._gkey[(t2[1]+1)%self._size][t2[0]][0]
            # Misma columna
            elif t1[1] == t2[1]:
                self._ctext += self._gkey[t1[1]][(t1[0]+1)%self._size][0] \
                               + self._gkey[t2[1]][(t2[0]+1)%self._size][0]
            # Diferente fila y columna
            else:
                self._ctext += self._gkey[t1[1]][t2[0]][0] \
                               + self._gkey[t2[1]][t1[0]][0]

    def __cipher_hill(self):
        """
        Cifra el texto con el cifrado de hill
        """
        text = self._text
        diff = len(text)%self._size
        for i in range(diff):
            text += 'X'
        self._ctext = ''
        tuples = []
        for i in range(0,len(text),self._size):
            iletters = text[i:i+self._size]
            t = []
            for l in iletters:
                t.append(self._alphabet.index(l))
            tuples.append(t)
        for t in tuples:
            result = multiply(self._gkey,t,len(self._alphabet))
            for j in range(len(result)):
                self._ctext += self._alphabet[result[j]]

    def __cipher_des(self):
        """
        Cifra el texto con DES.
        Nótese que va a tratar a su texto como texto en hexadecimal
        siempre que pueda (si solo usan los caracteres
        [0123456789ABCDEF] en el texto), en otro caso lo trata como
        texto plano
        """
        self._ctext = ''
        message = message_format_hexadecimal(self._text)
        submessages = string_to_substrings(message)
        Mn = []
        for submessage in submessages:
            bin_submessage = bin(int(submessage,16))[2:].zfill(64)
            permuted_submessage = ''
            for b in self._IP:
                permuted_submessage += bin_submessage[b-1]
            Mn.append(permuted_submessage)
        cipher_submessages = []
        for s in Mn:
            Ln = [s[:32]]
            Rn = [s[32:]]
            for n in range(1,17):
                Ln.append(Rn[n-1])
                r_expansion = ''
                for b in self._E_BIT_SELECTION: r_expansion += Rn[n-1][b-1]
                xor = ''.join(str(int(a) ^ int(b))\
                              for a, b in zip(r_expansion,self._gkey[4][n-1]))
                C = ''
                for i in range(8):
                    nbyte = xor[i*6:(i+1)*6]
                    row = int(nbyte[0]+nbyte[-1],2)
                    col = int(nbyte[1:-1],2)
                    C += bin(self._SBOXES[i][(row*16)+col])[2:].zfill(4)
                permutation_f = ''
                for b in self._P: permutation_f += C[b-1]
                Rn.append(''.join(str(int(a) ^ int(b))\
                                  for a, b in zip(Ln[n-1],permutation_f)))
            RL = Rn[-1]+Ln[-1]
            bin_cipher = ''
            for b in self._IP_INV: bin_cipher += RL[b-1]
            cipher_submessages.append(bin_cipher)
        for cs in cipher_submessages:
            self._ctext += hex(int(cs, 2))[2:].zfill(16).upper()

class Decipher(Coder):
    """
    Clase que puede desifrar textos
    """
    def __init__(self,cipher,key=None,alphabet=None):
        """
        Constructor que puede o no recibir una llave

        Args:
          cipher: El nombre del cifrado -> str
          alphabet: Todas las letras que usa el cifrado -> str
          key: Algo que representa la llave que se usa para el cifrado
               -> str/tuple(str,str)

        Returns:
          Un objeto que instancia a Decipher
        """
        super().__init__(cipher,key,alphabet)

    def decipher(self):
        """
        Sirve para decifrar el texto de acuerdo al cifrado especificado
        """
        if self._cipher == 'cesar':
            self.__decipher_cesar()
        elif self._cipher == 'afin':
            self.__decipher_afin()
        elif self._cipher == 'vigenere':
            self.__decipher_vigenere()
        if self._cipher == 'playfair':
            self.__decipher_playfair()
        if self._cipher == 'hill':
            self.__decipher_hill()
        if self._cipher == 'des':
            self.__decipher_des()

    def __decipher_cesar(self):
        """
        Descifra el texto con el cifrado de cesar
        """
        self._text = ''
        for letter in self._ctext:
            self._text += self._alphabet[self._gkey.index(letter)]

    def __decipher_afin(self):
        """
        Descifra el texto con el cifrado de afin
        """
        self._text = ''
        #   x=a'(y-b)mod r
        # a'a=1mod r
        r = self._gkey[2]
        a = self._gkey[0]
        x = 0
        a0 = 1
        while a > 1:
            q = a // r
            temp = r
            r = a % r
            a = temp
            t = x
            x = a0 - q * x
            a0 = t
        a0 = a0 % self._gkey[2]
        func = lambda y: (a0 * (y - self._gkey[1])) % self._gkey[2]
        for letter in self._ctext:
            self._text += self._alphabet[func(self._alphabet.index(letter))]

    def __decipher_vigenere(self):
        """
        Descifra el texto con el cifrado de vigenere
        """
        self._text = ''
        i = 0
        for letter in self._ctext:
            self._text += self._alphabet[self._gkey[i].index(letter)]
            i = (i+1)%len(self._gkey)

    def __decipher_playfair(self):
        """
        Descifra el texto con el cifrado de playfair
        """
        self._text = ''
        tuples = []
        for i in range(0,len(self._ctext),2):
            try:
                tuples.append((self._ctext[i],self._ctext[i+1]))
            except IndexError:
                tuples.append((self._ctext[i],'X'))
        for t in tuples:
            t1 = (0,0,False)
            t2 = (0,0,False)
            for i in range(self._size):
                for j in range(self._size):
                    if (self._gkey[i][j][0] == t[0]
                            or self._gkey[i][j][1] == t[0]):
                        t1 = (j,i,True)
                    if(self._gkey[i][j][0] == t[1]
                            or self._gkey[i][j][1] == t[1]):
                        t2 = (j,i,True)
                    if(t1[2] and t2[2]) == True: break
            # Misma letra en la pareja
            if (t1[0] == t2[0]) and (t1[1] == t2[1]):
                self._text += self._gkey[t1[1]][(t1[0]-1)%self._size][0] \
                              + self._gkey[t2[1]][(t2[0]-1)%self._size][0]
            # Misma fila
            elif t1[0] == t2[0]:
              self._text += self._gkey[(t1[1]-1)%self._size][t1[0]][0] \
                            + self._gkey[(t2[1]-1)%self._size][t2[0]][0]
            # Misma columna
            elif t1[1] == t2[1]:
              self._text += self._gkey[t1[1]][(t1[0]-1)%self._size][0] \
                            + self._gkey[t2[1]][(t2[0]-1)%self._size][0]
            # Diferente fila y columna
            else:
              self._text += self._gkey[t1[1]][t2[0]][0] \
                            + self._gkey[t2[1]][t1[0]][0]

    def __decipher_hill(self):
        """
        Descifra el texto con el cifrado de hill
        """
        text = self._ctext
        diff = len(text)%self._size
        for i in range(diff):
            text += 'X'
        self._text = ''
        tuples = []
        for i in range(0,len(text),self._size):
            iletters = text[i:i+self._size]
            t = []
            for l in iletters:
                t.append(self._alphabet.index(l))
            tuples.append(t)
        # Modificar para matrices de mas de 2x2
        inv = inverse(self._gkey,len(self._alphabet))
        for t in tuples:
            result = multiply(inv,t,len(self._alphabet))
            for j in range(len(result)):
                self._text += self._alphabet[result[j]]

    def __decipher_des(self):
        """
        Decifra el texto cifrado con DES
        Nótese que va a tratar a su texto cifrado como texto en
        hexadecimal siempre que pueda (si solo usan los caracteres
        [0123456789ABCDEF] en el texto), en otro caso lo trata como
        texto plano
        """
        self._text = ''
        message = message_format_hexadecimal(self._ctext)
        submessages = string_to_substrings(message)
        Mn = []
        for submessage in submessages:
            bin_submessage = bin(int(submessage,16))[2:].zfill(64)
            permuted_submessage = ''
            for b in self._IP:
                permuted_submessage += bin_submessage[b-1]
            Mn.append(permuted_submessage)
        cipher_submessages = []
        for s in Mn:
            Ln = [s[:32]]
            Rn = [s[32:]]
            for n in range(1,17):
                Ln.append(Rn[n-1])
                r_expansion = ''
                for b in self._E_BIT_SELECTION: r_expansion += Rn[n-1][b-1]
                # Se cambia el orden en que se usan las llaves para descifrar
                xor = ''.join(str(int(a) ^ int(b))\
                            for a, b in zip(r_expansion,self._gkey[4][16-n]))
                C = ''
                for i in range(8):
                    nbyte = xor[i*6:(i+1)*6]
                    row = int(nbyte[0]+nbyte[-1],2)
                    col = int(nbyte[1:-1],2)
                    C += bin(self._SBOXES[i][(row*16)+col])[2:].zfill(4)
                permutation_f = ''
                for b in self._P: permutation_f += C[b-1]
                Rn.append(''.join(str(int(a) ^ int(b))\
                                  for a, b in zip(Ln[n-1],permutation_f)))
            RL = Rn[-1]+Ln[-1]
            bin_cipher = ''
            for b in self._IP_INV: bin_cipher += RL[b-1]
            cipher_submessages.append(bin_cipher)
        for cs in cipher_submessages:
            self._text += hex(int(cs, 2))[2:].zfill(16).upper()
