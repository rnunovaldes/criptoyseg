from enum import Enum

class Color(Enum):
    """
    Enumeración que define una serie de constantes para colores de texto
    y fondo en la terminal; están definidos como cadenas de escape ANSI
    """
    RESET = '\033[0m'
    # Colores normales para el texto
    NEGRO = '\033[0;30m'
    ROJO = '\033[0;31m'
    VERDE = '\033[0;32m'
    AMARILLO = '\033[0;33m'
    AZUL = '\033[0;34m'
    MAGENTA = '\033[0;35m'
    CIAN = '\033[0;36m'
    BLANCO = '\033[0;37m'
    # Resaltado en negritas de los caracteres
    NEGRO_NEGRITAS = '\033[1;30m'
    ROJO_NEGRITAS = '\033[1;31m'
    VERDE_NEGRITAS = '\033[1;32m'
    AMARILLO_NEGRITAS = '\033[1;33m'
    AZUL_NEGRITAS = '\033[1;34m'
    MAGENTA_NEGRITAS = '\033[1;35m'
    CIAN_NEGRITAS = '\033[1;36m'
    BLANCO_NEGRITAS = '\033[1;37m'
    # Subrayado de los caracteres
    NEGRO_SUBRAYADO = '\033[4;30m'
    ROJO_SUBRAYADO = '\033[4;31m'
    VERDE_SUBRAYADO = '\033[4;32m'
    AMARILLO_SUBRAYADO = '\033[4;33m'
    AZUL_SUBRAYADO = '\033[4;34m'
    MAGENTA_SUBRAYADO = '\033[4;35m'
    CIAN_SUBRAYADO = '\033[4;36m'
    BLANCO_SUBRAYADO = '\033[4;37m'
    # Fondo de los carateres
    NEGRO_FONDO = '\033[40m'
    ROJO_FONDO = '\033[41m'
    VERDE_FONDO = '\033[42m'
    AMARILLO_FONDO = '\033[43m'
    AZUL_FONDO = '\033[44m'
    MAGENTA_FONDO = '\033[45m'
    CIAN_FONDO = '\033[46m'
    BLANCO_FONDO = '\033[47m'
    # Colores brillantes para el texto
    NEGRO_BRILLANTE = '\033[0;90m'
    ROJO_BRILLANTE = '\033[0;91m'
    VERDE_BRILLANTE = '\033[0;92m'
    AMARILLO_BRILLANTE = '\033[0;93m'
    AZUL_BRILLANTE = '\033[0;94m'
    MAGENTA_BRILLANTE = '\033[0;95m'
    CIAN_BRILLANTE = '\033[0;96m'
    BLANCO_BRILLANTE = '\033[0;97m'
    # Resaltado en negritas de los caracteres
    NEGRO_NEGRITAS_BRILLANTE = '\033[1;90m'
    ROJO_NEGRITAS_BRILLANTE = '\033[1;91m'
    VERDE_NEGRITAS_BRILLANTE = '\033[1;92m'
    AMARILLO_NEGRITAS_BRILLANTE = '\033[1;93m'
    AZUL_NEGRITAS_BRILLANTE = '\033[1;94m'
    MAGENTA_NEGRITAS_BRILLANTE = '\033[1;95m")'
    CIAN_NEGRITAS_BRILLANTE = '\033[1;96m'
    BLANCO_NEGRITAS_BRILLANTE = '\033[1;97m'
    # Fondo de los carateres
    NEGRO_FONDO_BRILLANTE = '\033[0;100m'
    ROJO_FONDO_BRILLANTE = '\033[0;101m'
    VERDE_FONDO_BRILLANTE = '\033[0;102m'
    AMARILLO_FONDO_BRILLANTE = '\033[0;103m'
    AZUL_FONDO_BRILLANTE = '\033[0;104m'
    MAGENTA_FONDO_BRILLANTE = '\033[0;105m'
    CIAN_FONDO_BRILLANTE = '\033[0;106m'
    BLANCO_FONDO_BRILLANTE = '\033[0;107m'

    @staticmethod
    def colorize(color, text):
        """
        Regresa una cadena dada con el color indicado

        Args:
          color: El color con es que se escribe el texto -> Color
          text: Texto a escribir -> str
        """
        return f'{color.value}{text}\033[0m'

class SpecialCharacter(Enum):
    """
    Enumeración con caracteres especiales que pueden ser usados en la
    terminal; están definidos como cadenas de escape ANSI
    """
    # Caracteres especiales que esperan a ser completados al darles un
    # formato (un numero natural)
    UP = '\u001B[{}A' #0
    DOWN = '\u001B[{}B' #1
    RIGHT = '\u001B[{}C' #2
    LEFT = '\u001B[{}D' #3
    NEXT_LINE = '\u001B[{}E' #4
    PREV_LINE = '\u001B[{}F' #5
    # Caracteres especiales qe ya están listos para ser usados con value
    # directamente
    CLEAR = '\u001B[2J' #6
    CLEAR_FROM_CURSOR = '\u001B[0J' #7
    CLEAR_CURRENT_LINE = '\u001B[2K' #8
    SAVE_CURSOR = '\u001b[s' #9
    REST_CURSOR = '\u001b[u' #10
    # Caracteres especiales qe ya están listos para ser usados con value
    # directamente y si se repiten de manera consecutiva tienen sentido
    NEW_LINE = '\n' #11
    BACKSPACE = '\b' #12

    def getCode(self, n):
        """
        Regresa una cadena para ser usada como caracter especial que
        desplaza una cantidad de veces indicada si el elemento lo permite

        Args:
          n: El número de desplazamiento según el caracter especial
             (que debe dársele formato)

        Return:
          Una cadena de caracteres especiales para ser usada -> str
        """
        listSP = list(SpecialCharacter)
        if self in listSP[:6]: return (self.value.format(n))
        elif self in listSP[6:11]: return (self.value)
        else: return (self.value * n)

class Printer():
    """
    Colección de funciones imprimir cadenas con colores predeterminados
    """
    @staticmethod
    def printError(str, end='\n'):
        """
        Imprime en consola, de color rojo las letras y fondo negro

        Args:
          str: La cadena a ser impresa -> str
          end: Lo que se pondrá al final de la cadena.
               Por defecto un salto de línea -> str
        """
        print(Color.ROJO.value, Color.NEGRO_FONDO.value, str,
              Color.RESET.value, sep='', end=end)

    @staticmethod
    def printWarning(str, end='\n'):
        """
        Imprime en consola, de color amarillo las letras y fondo negro

        Args:
          str: La cadena a ser impresa -> str
          end: Lo que se pondrá al final de la cadena.
               Por defecto un salto de línea -> str
        """
        print(Color.AMARILLO.value, Color.NEGRO_FONDO.value, str,
              Color.RESET.value, sep='', end=end)

    @staticmethod
    def printOk(str, end='\n'):
        """
        Imprime en consola, de color verde las letras y fondo negro

        Args:
          str: La cadena a ser impresa -> str
          end: Lo que se pondrá al final de la cadena.
               Por defecto un salto de línea -> str
        """
        print(Color.VERDE.value, Color.NEGRO_FONDO.value, str,
              Color.RESET.value, sep='', end=end)

    @staticmethod
    def printInfo(str, end='\n'):
        """
        Imprime en consola, de color azul las letras y fondo negro

        Args:
          str: La cadena a ser impresa -> str
          end: Lo que se pondrá al final de la cadena.
               Por defecto un salto de línea -> str
        """
        print(Color.CIAN.value, Color.NEGRO_FONDO.value, str,
              Color.RESET.value, sep='', end=end)

    @staticmethod
    def printExtra(str, end='\n'):
        """
        Imprime en consola, de color magenta las letras y fondo negro

        Args:
          str: La cadena a ser impresa -> str
          end: Lo que se pondrá al final de la cadena.
               Por defecto un salto de línea -> str
        """
        print(Color.MAGENTA.value, Color.NEGRO_FONDO.value, str,
              Color.RESET.value, sep='', end=end)

class Reader():
    """
    Solo es una función que usa input() pero le da unas
    características estáticas
    """
    @staticmethod
    def input(erase_line=True):
        """
        Es input() que tiene como parámetro '> ' de color azul con fondo
        negro

        Args:
          erase_line: Indica si se debe borrar lo escrito por el usuario
                      en cuanto dé enter. Por defecto lo hace -> bool

        Return:
          La cadena que se escribió -> str
        """
        if erase_line:
            str = input(Color.CIAN_NEGRITAS.value + Color.NEGRO_FONDO.value
                        + '> ' + Color.RESET.value)
            print(SpecialCharacter.PREV_LINE.getCode(1),
			      SpecialCharacter.CLEAR_FROM_CURSOR.value, sep='', end ='')
            return str
        return input(Color.CIAN_NEGRITAS.value + Color.NEGRO_FONDO.value
                     + '> ' + Color.RESET.value)
