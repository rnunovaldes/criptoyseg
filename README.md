# Criptografía y Seguridad - Repositorio

Repositorio que tiene scripts de python encargados como prácticas de varios
algoritmos de cifrados durante el curso de Criptografía y Seguridad de la
Facultad de Ciencias

## Uso del script
Importar lo necesario en el programa. Por ejemplo
```python
from coder import Cipher, Decipher
from ansii import Printer, Color
```

Se trabajó con python3 (espero que funcione también con python2, aunque no fue
probado)

## Organización de directorios
Se recomienda la creación de dos directorios, `input` y `output` si es que se
planea hacer uso de las rutas por defecto de lectura y escritura de archivos.
```
/
├── input/
│   └── ...
├── output/
│   └── ...
├── ansii.py
└── coder.py
```

### Autor
- Raúl N. Valdés
